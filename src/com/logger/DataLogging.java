package com.logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Aspect
public class DataLogging {
	@Autowired
	Logger logger  = LoggerFactory.getLogger(this.getClass());
	//Handler ch = new ConsoleHandler();
	//logger.getLogger("").addHandler(ch);
	//log4j.appender.FILE=org.apache.log4j.FileAppender;
	//log4j.appender.FILE.File=${log}/log.out	;
	@After(value="using(com..*)")
	public void after(JoinPoint jp) {
		logger.info("radio");
	}
}

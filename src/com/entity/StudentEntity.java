package com.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@javax.persistence.Table(name="EnrollStudent")
public class StudentEntity {
	@Id
	@GeneratedValue
	private Integer enrollNumber;
	private String studentName;
	private String gender;
	private String courseType;
	@ManyToOne
	@JoinColumn(name="courseCode")
	private CourseEntity course;
	
	public Integer getEnrollNumber() {
		return enrollNumber;
	}
	
	public void setEnrollNumber(Integer enrollNumber) {
		this.enrollNumber = enrollNumber;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}

	public CourseEntity getCourse() {
		return course;
	}
	
	public void setCourse(CourseEntity course) {
		this.course = course;
	}
	
	public String getCourseType() {
		return courseType;
	}
	
	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}
	
	public String getStudentName() {
		return studentName;
	}
	
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
}

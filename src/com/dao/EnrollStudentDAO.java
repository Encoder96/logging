package com.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bean.Course;
import com.bean.Student;
import com.entity.CourseEntity;
import com.entity.StudentEntity;

@Component
public class EnrollStudentDAO {
	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public int enrollStudent(Student student) {
		StudentEntity sEntity = new StudentEntity();
		sEntity.setStudentName(student.getStudentName());
		sEntity.setGender(student.getGender());
		if(student.getCourseType().length == 2)
			sEntity.setCourseType("Both");
		else
			sEntity.setCourseType(student.getCourseType()[0]);
		CourseEntity cEntity = new CourseEntity();
		cEntity.setCourseCode(student.getCourseCode());
		cEntity.setCourseType(sEntity.getCourseType());
		sEntity.setCourse(cEntity);
		em.persist(sEntity);
		return sEntity.getEnrollNumber();
	}
	
	public String retrieveCourseType(String courseCode) {
		CourseEntity cEntity = em.find(CourseEntity.class, courseCode);	 
		return cEntity.getCourseType();
	}
	
	public List<Course> getCourseDetails() {
		Query q = em.createQuery("select c from CourseEntity c"); 
		List<CourseEntity> cList = q.getResultList();		
		List<Course> courseList = new ArrayList<>();
		for(CourseEntity cEntity : cList) {
			Course course = new Course();
			course.setCourseCode(cEntity.getCourseCode());
			course.setCourseType(cEntity.getCourseType());
			course.setCourseName(cEntity.getCourseName());
			course.setCourseFees(String.valueOf(cEntity.getCourseFees()));
			courseList.add(course);
		}
		return courseList;
	}
}

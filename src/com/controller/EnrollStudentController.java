package com.controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bean.Course;
import com.bean.Student;
import com.exception.StudentEnrollmentException;
import com.service.EnrollStudentService;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletRequest;


@Controller
public class EnrollStudentController {
	
	@Autowired
	private EnrollStudentService service;
	
	@RequestMapping("/home.htm")
	public ModelAndView displayHomePage() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("home");
		return mv;
	}

	@RequestMapping("/student.htm")
	public ModelAndView displayDetailsPage() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("student");
		return mv;
	}
		
	@ModelAttribute("STUDENT")
	public Map<String, String> populateCourse() {
		Map<String, String> courseMap = new HashMap<>();
		List<Course> courseList =(List<Course>) service.getCourseDetails();
		for(Course course : courseList) {
			courseMap.put(course.getCourseCode(), course.getCourseName());
		}
		return courseMap;
	}
	
	@RequestMapping("/enroll.htm")
	public ModelAndView loadEnrollPage(ModelMap map) {
		ModelAndView mv = new ModelAndView();
		Student student = new Student();
		map.addAttribute("STUD", student);
		mv.setViewName("courseenrollment");
		return mv;		
	}
	
	@RequestMapping("/enrolls.htm")
	public ModelAndView enrollStudent(@ModelAttribute("STUD")Student student) {
		ModelAndView mv = new ModelAndView();
		try {
			String eno = service.enrollStudent(student);
			mv.addObject("Message", "Thank you for your registration. Your enrollment no is " + eno);
			mv.setViewName("success");			
		} catch (StudentEnrollmentException e) {
			// TODO Auto-generated catch block
			mv.addObject("Message", e.getMessage());			
			mv.setViewName("courseenrollment");
		}
		return mv;		
	}
	
	@RequestMapping("/vcourse.htm")
	public ModelAndView getCourseDetails() {
		ModelAndView mv = new ModelAndView();
		List<Course> courseList = service.getCourseDetails();
		mv.addObject("COURSE", courseList);
		mv.setViewName("viewallcourse");
		return mv;		
	}
}

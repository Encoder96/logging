package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bean.Course;
import com.bean.Student;
import com.dao.EnrollStudentDAO;
import com.entity.StudentEntity;
import com.exception.StudentEnrollmentException;

@Component
public class EnrollStudentService {
	@Autowired
	private EnrollStudentDAO dao;
	
	public String enrollStudent(Student student) throws StudentEnrollmentException {
		String cType = dao.retrieveCourseType(student.getCourseCode());
		if(student.getCourseType().length == 2) {
			if(cType == "Both")
				return String.valueOf(dao.enrollStudent(student));
			else
				throw new StudentEnrollmentException();
		}
		else {
			if(student.getCourseType()[0].toLowerCase() == cType.toLowerCase())
				return String.valueOf(dao.enrollStudent(student));
			else
				throw new StudentEnrollmentException();
		}
	}
	
	public List<Course> getCourseDetails() {
		return dao.getCourseDetails();
	}
}

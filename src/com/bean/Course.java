package com.bean;

//change
public class Course {
	private String courseCode;
	private String courseName;
	private String courseType;
	private String courseFees;
	public String getCourseCode() {
		return courseCode;
	}
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCourseType() {
		return courseType;
	}
	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}
	public String getCourseFees() {
		return courseFees;
	}
	public void setCourseFees(String double1) {
		this.courseFees = double1;
	}
}

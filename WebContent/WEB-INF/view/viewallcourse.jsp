<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<center>
		<h2> All Courses </h2> <br>
		<table borser="3">
			<tr>
				<th> Course Name </th>
				<th> Course Fees </th>
				<th> Course Type </th>				
			</tr>
			<c:forEach var="i" items="${COURSE}">
				<tr>
					<td> <c:out value="${i.courseName}"></c:out> </td>
					<td> <c:out value="${i.courseFees}"></c:out> </td>
					<td> <c:out value="${i.courseType}"></c:out> </td>					
				</tr>
			</c:forEach>
		</table>
	</center>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<center>
		<h2> Course Enrollment </h2> <br>
		<form:form action="enrolls.htm" modelAttribute="STUD">
			<table border="3">
			<tr>
				<td>Student Name:</td>
				<td><form:input path="studentName"/></td>
			</tr>
			<tr>
				<td>Course Name:</td>
				<td><form:select path="courseCode">
					<form:options items="${STUDENT}"/>
				</td>
			</form:select>
			</tr>
			<tr>
				<td>Gender:</td>
				<td><form:radiobutton path="gender" value="M"/> Male
				    <form:radiobutton path="gender" value="F"/> Female
				</td>    
		    </tr>
		    <tr>
		    	<td>Course Type:</td>
		    	<td><form:checkbox path="courseType" value="elearning"/>e-learning
		    		<form:checkbox path="courseType" value="classroom"/>classroom
		    	</td>	
		    </tr>			
		   </table> 		
		   <br>	
		<input type="submit" Value="Enroll"> <br>    			
		</form:form>
		<h4> ${Message}</h4>
		<br>
		<a href="student.htm"> Back </a>					   		
	</center>
</body>
</html>